/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.UserDAO;
import DTO.UserDTO;
import UserValidator.UserErrorMessage;
import UserValidator.UserValidator;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author khang
 */
@WebServlet(name = "SignUpServlet", urlPatterns = {"/SignUpServlet"})
public class SignUpServlet extends HttpServlet {
    private final String LOGIN_PAGE = "login.jsp";
    private final String ERROR_PAGE = "";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = LOGIN_PAGE;
        try  {
            boolean valid = true;
            String phone = request.getParameter("txtPhone");
            String password = request.getParameter("txtPassword");
            String fullName = request.getParameter("txtFullname");
            String address = request.getParameter("txtAddress");
            String dob = request.getParameter("DOB");
            Date date = Date.valueOf(dob);
            String email = request.getParameter("email");
            
            UserValidator userValidator = new UserValidator();
            UserErrorMessage userErrorMessage = new UserErrorMessage();
            if (phone.isEmpty()){
                userErrorMessage.setPhoneErrorMsg("Phone number must contain 10 digit");
                valid = false;
            }
            System.out.println(phone);
            if (password.equals("")){
                userErrorMessage.setPasswordErrorMsg("Password must longer than 8 characters");
                valid = false;
            }
            if (password.equals("")){
                System.out.println("pass null");
            }
            if (userValidator.dobValidator(date) == false){
                userErrorMessage.setDobErrorMsg("DOB must be older than 18 years old");
                valid = false;
            }
            if (userValidator.emailValidate(email) == false){
                userErrorMessage.setEmailErrorMsg("");
                valid = false;
            }
            if (userValidator.fullnameValidate(fullName) == false){
                userErrorMessage.setEmailErrorMsg("");
                valid = false;
            }
            UserDAO userDAO = new UserDAO();
            System.out.println(valid);
            if (valid){
                UserDTO userDTO = new UserDTO(0, fullName, date, address, phone, email, password, null, null);
                if (userDAO.registerAccount(userDTO) == true){
                    
                }
                request.setAttribute("REGISTER_SUCCESS", "Register success");
                System.out.println("Sign up success");
            }
            else {
                System.out.println(password);
                System.out.println("Sign Up servlet");
            }
            
//            System.out.println(phone);
//            System.out.println(password);
//            System.out.println(fullName);
//            System.out.println(address);
//            System.out.println(date);
//            System.out.println(email);
            url = LOGIN_PAGE;
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(SignUpServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SignUpServlet.class.getName()).log(Level.SEVERE, null, ex);
        }        
        finally{
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
