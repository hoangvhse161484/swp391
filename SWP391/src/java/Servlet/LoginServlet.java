/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.UserDAO;
import DTO.UserDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author khang
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {
    private final String LOGIN_SUCCESS_CUSTOMER = "pageCustomer.jsp";
    private final String LOGIN_SUCCESS_ADMIN = "pageAdmin.jsp";
    private final String LOGIN_SUCCESS_TRANSPORTATION_COMPANY = "pageCompany.jsp";
    private final String LOGIN_PAGE = "login.jsp";
    private final String LOGIN_FAIL = "";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = LOGIN_PAGE;
        try  {
            String phone = request.getParameter("txtPhone");
            String password = request.getParameter("txtPassword");
            UserDAO dao = new UserDAO();
            UserDTO userDTO = dao.checkLogin(phone, password);
            HttpSession session = request.getSession();
            if (userDTO != null) {
                if (userDTO.getRoleDTO().getRoleName().equals("AD")){
                    session.setAttribute("ADMIN", userDTO);
                    url = LOGIN_SUCCESS_ADMIN;
                }
                else if (userDTO.getRoleDTO().getRoleName().equals("US")){
                    session.setAttribute("CUSTOMER", userDTO);
                    url = LOGIN_SUCCESS_CUSTOMER;
                }
                else if (userDTO.getRoleDTO().getRoleName().equals("TC")){
                    session.setAttribute("TRANSPORTATIONCOMPANY", userDTO);
                    url = LOGIN_SUCCESS_TRANSPORTATION_COMPANY;
                }
                
               
            }
            else {
                request.setAttribute("LoginFail", "Invalid phone or password");
                url = LOGIN_PAGE;
                
            }

        }
        catch (ClassNotFoundException ex) {        
            log ("Class not found exception: " + ex.getMessage());
        } catch (SQLException ex) {
            log ("SQL Exception : " + ex.getMessage());
        }        
        finally {
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
