/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.UserDAO;
import DTO.UserDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author khang
 */
@WebServlet(name = "ServletDispatcher", urlPatterns = {"/ServletDispatcher"})
public class ServletDispatcher extends HttpServlet {
    private final String LOGIN_PAGE = "login.jsp";
    private final String LOGIN_CONTROLLER = "LoginServlet";
    private final String SIGNUP_CONTROLLER = "SignUpServlet";
    private final String LOGOUT_CONTROLLER = "LogoutServlet";
    private final String REGISTER_CONTROLLER= "RegisterServlet";
    private final String VIEW_LIST_TRIP_CONTROLLER = "ViewListTripServlet";
    private final String VIEW_LIST_RENTAL_CAR_CONTROLLER = "ViewListRentalCarServlet";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String button = request.getParameter("btnAction");
        String url = LOGIN_PAGE;
        
        try {
            if ("Login".equals(button)){
                url = LOGIN_CONTROLLER;
            }
            else if ("Register".equals(button)){
                url = SIGNUP_CONTROLLER;
                System.out.println("sign up");
               
            }

            else if ("ViewListTrip".equals(button)){
                url = VIEW_LIST_TRIP_CONTROLLER;
            }
            else if ("ViewListRentalCar".equals(button)){
                url = VIEW_LIST_RENTAL_CAR_CONTROLLER;
            }
             
        } 
        finally{
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
