/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.PriceDTO;
import DTO.TypeOfPriceDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utils.DBUtils;

/**
 *
 * @author khang
 */
public class PriceDAO {
    public PriceDTO getPriceByID (int id) throws ClassNotFoundException, SQLException{
        PriceDTO priceDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select priceID,price,amount,TypeID,totalPrice  "
                        + "from tblPrice "
                        + "where priceID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()){
                    int priceID = rs.getInt("priceID");
                    float price = rs.getFloat("price");
                    int amount = rs.getInt("amount");
                    int TypeID = rs.getInt("TypeID");
                    float totalPrice = rs.getFloat("totalPrice");
                    TypeOfPriceDTO typeOfPriceDTO = getTypeOfPriceByID(TypeID);
                    if (typeOfPriceDTO != null){
                        priceDTO = new PriceDTO(priceID, price, amount, typeOfPriceDTO, totalPrice);
                    }
                }
            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return priceDTO;
    }
    
    public TypeOfPriceDTO getTypeOfPriceByID (int id) throws ClassNotFoundException, SQLException{
        TypeOfPriceDTO typeOfPriceDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select TypeID,TypeName "
                        + "from tblTypeOfPrice "
                        + "where TypeID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()){
                    int TypeID = rs.getInt("TypeID");
                    String TypeName = rs.getString("TypeName");
                    typeOfPriceDTO = new TypeOfPriceDTO(TypeID, TypeName);
                }

            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return  typeOfPriceDTO;
    }
}
