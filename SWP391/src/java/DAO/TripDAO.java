/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;


import DTO.CarRentalDTO;
import DTO.DestinationDTO;
import DTO.TripDTO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utils.DBUtils;

/**
 *
 * @author khang
 */
public class TripDAO {
    public ArrayList<TripDTO> getAllTrip () throws ClassNotFoundException, SQLException{
        ArrayList<TripDTO> tripList = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select tripID,tripName,description,desID "
                        + "from tblTrip";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();

                    tripList = new ArrayList<>();
                
                while (rs.next()){
                   int tripID = rs.getInt("tripID");
                   String tripName = rs.getString("tripName");
                   String description = rs.getString("description");
                   int desID = rs.getInt("desID");
                   DestinationDTO destinationDTO = getDestinationByID(desID);
                   if (destinationDTO != null){
                       TripDTO tripDTO = new TripDTO(tripID, tripName, description, destinationDTO);
                        tripList.add(tripDTO);
                   }
                   
                    }
                
            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        
        return tripList;
    }
    
    public DestinationDTO getDestinationByID (int id ) throws ClassNotFoundException, SQLException{
        DestinationDTO destinationDTO = null ;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs= null;
        try {
            con = DBUtils.getConnection();
            if (con != null){
                String sql = "select desID,startDes,endDes,startTime,arriveTime "
                        + "from tblDestination "
                        + "where desID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()){
                   int desID = rs.getInt("desID");
                   String startDes = rs.getString("startDes");
                   String endDes = rs.getString("endDes");
                   Date startTime = rs.getDate("startTime");
                   Date arriveTime = rs.getDate("arriveTime");
                   destinationDTO = new DestinationDTO(desID,startDes, endDes, startTime, arriveTime);
                }
            }
        } 
        finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null){
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        
        return destinationDTO;
    }
    public ArrayList<TripDTO> searchTrip (String searchValue) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs= null;
        ArrayList<TripDTO> result = null;
        try {
            con = DBUtils.getConnection();
            if (con != null){
                String sql = "select tripID,tripName,description,desID "
                        + "from tblTrip where tripName "
                        + "like ?";
                ps = con.prepareStatement(sql);
                ps.setString(1, searchValue);
                rs = ps.executeQuery();

                   result = new ArrayList<>();
                   while (rs.next()){
                       int id = rs.getInt("tripID");
                       String tripName = rs.getString("tripName");
                       String description = rs.getString("description");
                       int desId = rs.getInt("desID");
                       DestinationDTO destinationDTO = getDestinationByID(id);
                       TripDTO tripDTO = new TripDTO(desId, tripName, description, destinationDTO);
                       result.add(tripDTO);
                   }

            }
        } 
        finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null){
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return result;
    }
    
    public boolean addTrip(TripDTO tripDTO) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = DBUtils.getConnection();
            
            if (con != null) {
                
                String sql = "insert into tblTrip "
                        + "values (?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setString(1, tripDTO.getTripName());
                ps.setString(2, tripDTO.getDescription());
                ps.setInt(3, tripDTO.getDestinationDTO().getDesId());
                if (ps.executeUpdate() > 0){
                    return true;
                }
                
            }
        } 
        finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null){
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return false;
    }
    
}
