/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.CarRentalDTO;
import DTO.CarTypeDTO;
import DTO.DestinationDTO;
import DTO.LicenseVehicleDTO;
import DTO.PriceDTO;
import DTO.TripDTO;
import DTO.UserDTO;
import DTO.VehicleDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utils.DBUtils;

/**
 *
 * @author khang
 */
public class RentCarDAO {
    public ArrayList<CarRentalDTO> getAllRentalCar () throws ClassNotFoundException, SQLException{
        ArrayList<CarRentalDTO> listRentalCar = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        VehicleDAO vehicleDAO = null;
        PriceDAO priceDAO = null;
        UserDAO userDAO = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select rentCarID,vehicleID,priceID,licenseID,Status,userID "
                        + "from tblCarRental";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();

                    
                    vehicleDAO = new VehicleDAO();
                   priceDAO = new PriceDAO();
                   userDAO = new UserDAO();
                   listRentalCar = new ArrayList<>();

                
                while (rs.next()){
                  int rentCarID = rs.getInt("rentCarID");
      
                  int vehicleID = rs.getInt("vehicleID");

                  VehicleDTO vehicleDTO = vehicleDAO.getVehicleByID(vehicleID);
                  int priceID = rs.getInt("priceID");
                  PriceDTO priceDTO = priceDAO.getPriceByID(priceID);
                  int licenseID = rs.getInt("licenseID");
                  LicenseVehicleDTO licenseVehicleDTO = vehicleDAO.getLicenseVehicleByID(licenseID);
                  boolean Status = rs.getBoolean("Status");
                  int userID = rs.getInt("userID");
                  UserDTO userDTO = userDAO.getUserByID(userID);
                  if (vehicleDTO != null && priceDTO != null && licenseVehicleDTO != null ){
                      CarRentalDTO carRentalDTO = new CarRentalDTO(rentCarID, vehicleDTO, priceDTO, licenseVehicleDTO, Status, userDTO);
                        listRentalCar.add(carRentalDTO);
                  }

                    }
                
            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        
        return listRentalCar ;
    }
    
//    public ArrayList<CarRentalDTO> searchRentalCar (String searchValue) throws ClassNotFoundException, SQLException{
//        Connection con = null;
//        PreparedStatement ps = null;
//        ResultSet rs= null;
//        ArrayList<CarRentalDTO> result = null;
//        try {
//            con = DBUtils.getConnection();
//            if (con != null){
//                String sql = "select tripID,tripName,description,desID "
//                        + "from tblTrip where tripName "
//                        + "like ?";
//                ps = con.prepareStatement(sql);
//                ps.setString(1, searchValue);
//                rs = ps.executeQuery();
//                if (rs.next()){
//                   result = new ArrayList<>();
//                   while (rs.next()){
//                       int id = rs.getInt("tripID");
//                       String tripName = rs.getString("tripName");
//                       String description = rs.getString("description");
//                       int desId = rs.getInt("desID");
//                       DestinationDTO destinationDTO = getDestinationByID(id);
//                       TripDTO tripDTO = new TripDTO(desId, tripName, description, destinationDTO);
//                       result.add(tripDTO);
//                   }
//                }
//            }
//        } 
//        finally{
//            if (rs != null){
//                rs.close();
//            }
//            if (ps != null){
//                ps.close();
//            }
//            if (con != null){
//                con.close();
//            }
//        }
//    }
//    
    public ArrayList<CarTypeDTO> getAllCarType () throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs= null;
        ArrayList<CarTypeDTO> result = null;
        try {
            con = DBUtils.getConnection();
            if (con != null){
                String sql = "select carTypeID,carTypeName,numberOfSeat "
                        + "from tblCarType";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                result = new ArrayList<>();
                while (rs.next()){
                    int carTypeID = rs.getInt("carTypeID");
                    String carTypeName = rs.getString("carTypeName");
                    String numberOfSeat = rs.getString("numberOfSeat");
                    CarTypeDTO carTypeDTO = new CarTypeDTO(carTypeID, carTypeName, numberOfSeat);
                    result.add(carTypeDTO);
                }
            }
        } 
        finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null){
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return result;
    }
}
