/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.CarTypeDTO;
import DTO.DriverDTO;
import DTO.LicenseVehicleDTO;
import DTO.VehicleDTO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utils.DBUtils;

/**
 *
 * @author khang
 */
public class VehicleDAO {
    public VehicleDTO getVehicleByID (int id) throws ClassNotFoundException, SQLException{
        VehicleDTO vehicleDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select vehicleID,vehicleName,licensePlates,carTypeID "
                        + "from tblVehicle "
                        + "where vehicleID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()){
                    int vehicleID = rs.getInt("vehicleID");
                    String vehicleName = rs.getString("vehicleName");
                    String licensePlates = rs.getString("licensePlates");
                    int carTypeID = rs.getInt("carTypeID");
                    CarTypeDTO carTypeDTO = getCarTypeByID(carTypeID);
                    if (carTypeDTO != null){
                        vehicleDTO = new VehicleDTO(vehicleID, vehicleName, licensePlates, carTypeDTO);
                    }
                    
                }

            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return vehicleDTO;
    }
    
    public CarTypeDTO getCarTypeByID (int id) throws ClassNotFoundException, SQLException{
        CarTypeDTO carTypeDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select carTypeID,carTypeName,numberOfSeat "
                        + "from tblCarType "
                        + "where carTypeID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()){
                    int carTypeID = rs.getInt("carTypeID");
                    String carTypeName = rs.getString("carTypeName");
                    String numberOfSeat = rs.getString("numberOfSeat");
                    carTypeDTO = new CarTypeDTO(carTypeID, carTypeName, numberOfSeat);
                }
            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return carTypeDTO;
    }
    
        public LicenseVehicleDTO getLicenseVehicleByID (int id) throws ClassNotFoundException, SQLException{
        LicenseVehicleDTO licenseVehicleDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select licenseID, driverID, Class,ClassificationOfMotorVehicles,BeginningDate, Expires  "
                        + "from tblLicenseVehicle "
                        + "where licenseID = ? ";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()){
                    int licenseID = rs.getInt("licenseID");
                    int driverID = rs.getInt("driverID");
                    DriverDTO driverDTO = getDriverByID(driverID);
                    String Class = rs.getString("Class");
                    String ClassificationOfMotorVehicles = rs.getString("ClassificationOfMotorVehicles");
                    Date BeginningDate = rs.getDate("BeginningDate");
                    Date Expires = rs.getDate("Expires");
                    if (driverDTO != null){
                        licenseVehicleDTO = new LicenseVehicleDTO(driverDTO, Class, ClassificationOfMotorVehicles, BeginningDate, Expires);
                    }
                    
                }

            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return licenseVehicleDTO;
    }
        
    public DriverDTO getDriverByID (int id) throws ClassNotFoundException, SQLException{
        DriverDTO driverDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select driverID,driverName,dob,address,nationality "
                        + "from tblDriver "
                        + "where driverID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()){
                    int driverID = rs.getInt("driverID");
                    String driverName = rs.getString("driverName");
                    Date dob = rs.getDate("dob");
                    String address = rs.getString("address");
                    String nationality = rs.getString("nationality");
                    driverDTO = new DriverDTO(driverID, driverName, dob, address, nationality);
                }

            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
            
            return driverDTO;
        }
}
