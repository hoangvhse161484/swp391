/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.DepositDTO;
import DTO.RoleDTO;
import DTO.UserDTO;
import java.io.Console;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utils.DBUtils;

/**
 *
 * @author khang
 */
public class UserDAO {
    public ArrayList<UserDTO> getAllUserDTOs () throws SQLException, ClassNotFoundException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<UserDTO> userList = null;
        try {
            con = DBUtils.getConnection();
            
            if (con != null) {
                
                String sql = "select userID,fullName,dob,address,phone,email,password,wallet,roleID "
                        + "from tblUsers";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                if (rs.next()){
                    userList = new ArrayList<>();
                
                while (rs.next()){
                    int id = rs.getInt("userID");
                    String fullName = rs.getString("fullName");
                    Date dob = rs.getDate("dob");
                    String address = rs.getString("address");
                    String phone = rs.getString("phone");
                    String email = rs.getString("email");
                    int wallet = rs.getInt("wallet");
                    DepositDTO depositDTO = getDepositByID(wallet);
                    String password = rs.getString("password");
                    int roleID = rs.getInt("roleID");
                    RoleDTO roleDTO = getRoleByID(roleID);
                    if (roleDTO != null){
                        UserDTO dto = new UserDTO(id, fullName, dob, address, phone, email, password, depositDTO, roleDTO);
                        userList.add(dto);
                    }
                }
            }

                
            }
        } 
        finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null){
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return userList;
    }
    
    public RoleDTO getRoleByID (int roleID1) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        RoleDTO roleDTO = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "  select roleID,roleName from tblRoles where roleID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, roleID1);
                rs = ps.executeQuery();
                
                if (rs.next()){
                    int roleID = rs.getInt("roleID");

                    String roleName = rs.getString("roleName");
                    
                    roleDTO = new RoleDTO(roleID, roleName);
                }
            }
            
        } finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null){
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return roleDTO;
    }
    
    public DepositDTO getDepositByID (int id) throws SQLException, ClassNotFoundException{
        DepositDTO depositDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = DBUtils.getConnection();
            
            if (con != null) {
                
                String sql = "select depositID,deposit "
                        + "from tblDeposit where depositID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                
                if (rs.next()){
                    int depositid = rs.getInt("depositID");
                    float deposit = rs.getFloat("deposit");
                    depositDTO = new DepositDTO(id, deposit);
                }
                
            }
        } 
        finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null){
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return depositDTO;
    }
    
    
    
    public UserDTO checkLogin (String phone , String password) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        UserDTO userDTO = null;
        try {
            con = DBUtils.getConnection();
            
            if (con != null) {
                
                String sql = "select userID,fullName,dob,address,phone,email,wallet,password,roleID "
                        + "from tblUsers "
                        + "where phone = ? and password = ?";
                ps = con.prepareStatement(sql);
                ps.setString(1, phone);
                ps.setString(2, password);
                rs = ps.executeQuery();
                
                if (rs.next()){
                    int id = rs.getInt("userID");
                    String fullName = rs.getString("fullName");
                    Date dob = rs.getDate("dob");
                    String address = rs.getString("address");
                    String phoneAccount = rs.getString("phone");
                    String email = rs.getString("email");
                    int walletID = rs.getInt("wallet");
                    DepositDTO depositDTO = getDepositByID(walletID);
                    String passwordUser = rs.getString("password");
                    int roleID = rs.getInt("roleID");
                    RoleDTO roleDTO = getRoleByID(roleID);
                    if (depositDTO != null && roleDTO != null){
                        userDTO = new UserDTO(id, fullName, dob, address, phone, email, password, depositDTO, roleDTO);
                    }
                }
                
            }
        } 
        finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null){
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return userDTO;
    }
    
    public UserDTO getUserByID (int id) throws ClassNotFoundException, SQLException{
        UserDTO userDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = DBUtils.getConnection();
            
            if (con != null) {
                
                String sql = "select userID,fullName,dob,address,phone,email,wallet,password,roleID  "
                        + "from tblUsers "
                        + "where userID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                
                if (rs.next()){
                    int userID = rs.getInt("userID");
                    String fullName = rs.getString("fullName");
                    Date dob = rs.getDate("dob");
                    String address = rs.getString("address");
                    String phone = rs.getString("phone");
                    String email = rs.getString("email");
                    int wallet = rs.getInt("wallet");
                    DepositDTO depositDTO = getDepositByID(wallet);
                    String password = rs.getString("password");
                    int roleID = rs.getInt("roleID");

                    RoleDTO roleDTO = getRoleByID(roleID);
                    if (depositDTO != null && roleDTO!= null){
                        userDTO = new UserDTO(id, fullName, dob, address, phone, email, password, depositDTO, roleDTO);
                    }
                }
                
            }
        } 
        finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null){
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return userDTO;
    }
    
    public boolean registerAccount (UserDTO userDTO) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int walletID = 0;
        try {
            con = DBUtils.getConnection();
            
            if (con != null) {
                
                String sql = "insert into tblUsers "
                        + "values (?,?,?,?,?,?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setString(1, userDTO.getFullName());
                ps.setDate(2, userDTO.getDob());
                ps.setString(3, userDTO.getAddress());
                ps.setString(4, userDTO.getPhone());
                ps.setString(5, userDTO.getEmail());
                walletID = createWallet();
                if (walletID != -1 ){
                    ps.setInt(6, walletID);
                }
                
                ps.setString(7, userDTO.getPassword());
                ps.setInt(8, 1);
                if (ps.executeUpdate() > 0 ){
                    return true;
                }
                
            }
        } 
        finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null){
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return false;
    }
    public int createWallet () throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int walletID = -1;
        try {
            con = DBUtils.getConnection();
            
            if (con != null) {
                
                String sql = "insert into tblDeposit "
                        + "values (0)";
                ps = con.prepareStatement(sql);
                ps.executeUpdate();
                
                sql = "select @@IDENTITY as depositID";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                if (rs.next()){
                    walletID = rs.getInt("depositID");
                    
                }
                
            }
        } 
        finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null){
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return walletID;
    }
}
