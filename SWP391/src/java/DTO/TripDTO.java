/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author khang
 */
public class TripDTO {
    private int tripID ;
    private String tripName;
    private String description;
    private DestinationDTO destinationDTO;

    public TripDTO() {
    }

    public TripDTO(int tripID, String tripName, String description, DestinationDTO destinationDTO) {
        this.tripID = tripID;
        this.tripName = tripName;
        this.description = description;
        this.destinationDTO = destinationDTO;
    }


    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DestinationDTO getDestinationDTO() {
        return destinationDTO;
    }

    public void setDestinationDTO(DestinationDTO destinationDTO) {
        this.destinationDTO = destinationDTO;
    }

    public int getTripID() {
        return tripID;
    }

    public void setTripID(int tripID) {
        this.tripID = tripID;
    }
    
    
}
