/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author khang
 */
public class PosSeatDTO {
    private int seatPosID;
    private String seatPosName;

    public PosSeatDTO() {
    }

    public PosSeatDTO(int seatPosID, String seatPosName) {
        this.seatPosID = seatPosID;
        this.seatPosName = seatPosName;
    }

    public int getSeatPosID() {
        return seatPosID;
    }

    public void setSeatPosID(int seatPosID) {
        this.seatPosID = seatPosID;
    }



    public String getSeatPosName() {
        return seatPosName;
    }

    public void setSeatPosName(String seatPosName) {
        this.seatPosName = seatPosName;
    }
    
}
