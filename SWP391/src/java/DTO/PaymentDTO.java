/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author khang
 */
public class PaymentDTO {
    private int paymentID;
    private String paymentName;
    private PriceDTO priceDTO;
    private CarRentalDTO rentCarDTO;
    private TicketDTO ticketDTO;

    public PaymentDTO() {
    }

    public PaymentDTO(int paymentID, String paymentName, PriceDTO priceDTO, CarRentalDTO rentCarDTO, TicketDTO ticketDTO) {
        this.paymentID = paymentID;
        this.paymentName = paymentName;
        this.priceDTO = priceDTO;
        this.rentCarDTO = rentCarDTO;
        this.ticketDTO = ticketDTO;
    }



    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public PriceDTO getPriceDTO() {
        return priceDTO;
    }

    public void setPriceDTO(PriceDTO priceDTO) {
        this.priceDTO = priceDTO;
    }

    public CarRentalDTO getRentCarDTO() {
        return rentCarDTO;
    }

    public void setRentCarDTO(CarRentalDTO rentCarDTO) {
        this.rentCarDTO = rentCarDTO;
    }

    public TicketDTO getTicketDTO() {
        return ticketDTO;
    }

    public void setTicketDTO(TicketDTO ticketDTO) {
        this.ticketDTO = ticketDTO;
    }

    public int getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(int paymentID) {
        this.paymentID = paymentID;
    }
    
     
}
