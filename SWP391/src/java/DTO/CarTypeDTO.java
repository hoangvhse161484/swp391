/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;

/**
 *
 * @author khang
 */
public class CarTypeDTO implements Serializable{
    int carTypeID;
    private String carTypeName;
    private  String numberOfSeat;

    public CarTypeDTO() {
    }

    public CarTypeDTO(int carTypeID, String carTypeName, String numberOfSeat) {
        this.carTypeID = carTypeID;
        this.carTypeName = carTypeName;
        this.numberOfSeat = numberOfSeat;
    }



    

    public String getCarTypeName() {
        return carTypeName;
    }

    public void setCarTypeName(String carTypeName) {
        this.carTypeName = carTypeName;
    }

    public String getNumberOfSeat() {
        return numberOfSeat;
    }

    public void setNumberOfSeat(String numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }

    public int getCarTypeID() {
        return carTypeID;
    }

    public void setCarTypeID(int carTypeID) {
        this.carTypeID = carTypeID;
    }

    
    
    
}
