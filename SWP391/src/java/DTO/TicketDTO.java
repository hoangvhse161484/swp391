/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author khang
 */
public class TicketDTO {
    private int ticketID;
    private VehicleDTO vehicleDTO;
    private TripDTO tripDTO;
    private PriceDTO priceDTO;
    private PosSeatDTO posSeatDTO;
    private UserDTO userDTO;

    public TicketDTO(int ticketID, VehicleDTO vehicleDTO, TripDTO tripDTO, PriceDTO priceDTO, PosSeatDTO posSeatDTO, UserDTO userDTO) {
        this.ticketID = ticketID;
        this.vehicleDTO = vehicleDTO;
        this.tripDTO = tripDTO;
        this.priceDTO = priceDTO;
        this.posSeatDTO = posSeatDTO;
        this.userDTO = userDTO;
    }





    

    public TicketDTO() {
    }
    

    public VehicleDTO getVehicleDTO() {
        return vehicleDTO;
    }

    public void setVehicleDTO(VehicleDTO vehicleDTO) {
        this.vehicleDTO = vehicleDTO;
    }

    public TripDTO getTripDTO() {
        return tripDTO;
    }

    public void setTripDTO(TripDTO tripDTO) {
        this.tripDTO = tripDTO;
    }

    public PriceDTO getPriceDTO() {
        return priceDTO;
    }

    public void setPriceDTO(PriceDTO priceDTO) {
        this.priceDTO = priceDTO;
    }



   

    public PosSeatDTO getPosSeatDTO() {
        return posSeatDTO;
    }

    public void setPosSeatDTO(PosSeatDTO posSeatDTO) {
        this.posSeatDTO = posSeatDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public int getTicketID() {
        return ticketID;
    }

    public void setTicketID(int ticketID) {
        this.ticketID = ticketID;
    }
    
    
}
