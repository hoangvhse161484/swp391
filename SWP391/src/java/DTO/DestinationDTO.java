/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.sql.Date;

/**
 *
 * @author khang
 */
public class DestinationDTO {
    int desId;
    private String startDes;
    private String endDes;
    private Date startTime;
    private Date arriveTime;

    public DestinationDTO() {
    }

    public DestinationDTO(int desId, String startDes, String endDes, Date startTime, Date arriveTime) {
        this.desId = desId;
        this.startDes = startDes;
        this.endDes = endDes;
        this.startTime = startTime;
        this.arriveTime = arriveTime;
    }



    public String getStartDes() {
        return startDes;
    }

    public void setStartDes(String startDes) {
        this.startDes = startDes;
    }

    public String getEndDes() {
        return endDes;
    }

    public void setEndDes(String endDes) {
        this.endDes = endDes;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getArriveTime() {
        return arriveTime;
    }

    public void setArriveTime(Date arriveTime) {
        this.arriveTime = arriveTime;
    }

    public int getDesId() {
        return desId;
    }

    public void setDesId(int desId) {
        this.desId = desId;
    }
    
    
}
