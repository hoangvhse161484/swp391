/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.sql.Date;

/**
 *
 * @author khang
 */
public class LicenseVehicleDTO {
    private int licenseID;
    private DriverDTO driverDTO ;
    private String classDriver;
    private String classificationOfMotorVehicle;
    private Date beginningDate;
    private Date expires;

    public LicenseVehicleDTO() {
    }

    public LicenseVehicleDTO(DriverDTO driverDTO, String classDriver, String classificationOfMotorVehicle, Date beginningDate, Date expires) {
        this.driverDTO = driverDTO;
        this.classDriver = classDriver;
        this.classificationOfMotorVehicle = classificationOfMotorVehicle;
        this.beginningDate = beginningDate;
        this.expires = expires;
    }

    public DriverDTO getDriverDTO() {
        return driverDTO;
    }

    public void setDriverDTO(DriverDTO driverDTO) {
        this.driverDTO = driverDTO;
    }

    public String getClassDriver() {
        return classDriver;
    }

    public void setClassDriver(String classDriver) {
        this.classDriver = classDriver;
    }

    public String getClassificationOfMotorVehicle() {
        return classificationOfMotorVehicle;
    }

    public void setClassificationOfMotorVehicle(String classificationOfMotorVehicle) {
        this.classificationOfMotorVehicle = classificationOfMotorVehicle;
    }

    public Date getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(Date beginningDate) {
        this.beginningDate = beginningDate;
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }
    
    
}
