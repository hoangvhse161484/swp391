/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author khang
 */
public class PriceDTO {
    private int priceID;
    private float price;
    private int amount;
    private TypeOfPriceDTO typeOfPriceDTO;
    private float totalPrice;

    public PriceDTO() {
    }

    public PriceDTO(int priceID, float price, int amount, TypeOfPriceDTO typeOfPriceDTO, float totalPrice) {
        this.priceID = priceID;
        this.price = price;
        this.amount = amount;
        this.typeOfPriceDTO = typeOfPriceDTO;
        this.totalPrice = totalPrice;
    }

    public int getPriceID() {
        return priceID;
    }

    public void setPriceID(int priceID) {
        this.priceID = priceID;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public TypeOfPriceDTO getTypeOfPriceDTO() {
        return typeOfPriceDTO;
    }

    public void setTypeOfPriceDTO(TypeOfPriceDTO typeOfPriceDTO) {
        this.typeOfPriceDTO = typeOfPriceDTO;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

   
    
}
