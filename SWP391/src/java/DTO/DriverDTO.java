/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.sql.Date;

/**
 *
 * @author khang
 */
public class DriverDTO {
    private int driverID ;
    private String driverName ;
    private Date dob ;
    private String address;
    private String nationality;

    public DriverDTO() {
    }

    public DriverDTO(int driverID, String driverName, Date dob, String address, String nationality) {
        this.driverID = driverID;
        this.driverName = driverName;
        this.dob = dob;
        this.address = address;
        this.nationality = nationality;
    }


    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public int getDriverID() {
        return driverID;
    }

    public void setDriverID(int driverID) {
        this.driverID = driverID;
    }
    
    
}
