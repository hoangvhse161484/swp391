/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author khang
 */
public class TypeOfPriceDTO {
    int typeID;
    private String TypeName;

    public TypeOfPriceDTO() {
    }

    public TypeOfPriceDTO(int typeID, String TypeName) {
        this.typeID = typeID;
        this.TypeName = TypeName;
    }



    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String TypeName) {
        this.TypeName = TypeName;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }
    
    
}
