/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.sql.Date;

/**
 *
 * @author khang
 */
public class UserDTO {

    private int id;
    private String fullName;
    private Date dob;
    private String address;
    private String phone;
    private String email;
    private String password;
    private DepositDTO depositDTO;
    private RoleDTO roleDTO;

    public UserDTO(int id, String fullName, Date dob, String address, String phone, String email, String password, DepositDTO depositDTO, RoleDTO roleDTO) {
        this.id = id;
        this.fullName = fullName;
        this.dob = dob;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.depositDTO = depositDTO;
        this.roleDTO = roleDTO;
    }



    

    public UserDTO() {
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleDTO getRoleDTO() {
        return roleDTO;
    }

    public void setRoleDTO(RoleDTO roleDTO) {
        this.roleDTO = roleDTO;
    }

    public DepositDTO getDepositDTO() {
        return depositDTO;
    }

    public void setDepositDTO(DepositDTO depositDTO) {
        this.depositDTO = depositDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
