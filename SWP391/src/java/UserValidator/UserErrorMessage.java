/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserValidator;

/**
 *
 * @author khang
 */
public class UserErrorMessage {
    private String fullnameErrorMsg;
    private String phoneErrorMsg;
    private String dobErrorMsg;
    private String emailErrorMsg;
    private String passwordErrorMsg;

    public UserErrorMessage() {
    }

    public UserErrorMessage(String fullnameErrorMsg, String phoneErrorMsg, String dobErrorMsg, String emailErrorMsg, String passwordErrorMsg) {
        this.fullnameErrorMsg = fullnameErrorMsg;
        this.phoneErrorMsg = phoneErrorMsg;
        this.dobErrorMsg = dobErrorMsg;
        this.emailErrorMsg = emailErrorMsg;
        this.passwordErrorMsg = passwordErrorMsg;
    }

    public String getFullnameErrorMsg() {
        return fullnameErrorMsg;
    }

    public void setFullnameErrorMsg(String fullnameErrorMsg) {
        this.fullnameErrorMsg = fullnameErrorMsg;
    }

    public String getPhoneErrorMsg() {
        return phoneErrorMsg;
    }

    public void setPhoneErrorMsg(String phoneErrorMsg) {
        this.phoneErrorMsg = phoneErrorMsg;
    }

    public String getDobErrorMsg() {
        return dobErrorMsg;
    }

    public void setDobErrorMsg(String dobErrorMsg) {
        this.dobErrorMsg = dobErrorMsg;
    }

    public String getEmailErrorMsg() {
        return emailErrorMsg;
    }

    public void setEmailErrorMsg(String emailErrorMsg) {
        this.emailErrorMsg = emailErrorMsg;
    }

    public String getPasswordErrorMsg() {
        return passwordErrorMsg;
    }

    public void setPasswordErrorMsg(String passwordErrorMsg) {
        this.passwordErrorMsg = passwordErrorMsg;
    }

    
    
}
