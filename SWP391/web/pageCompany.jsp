<%-- 
    Document   : home
    Created on : Oct 25, 2022, 9:30:55 PM
    Author     : PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    </head>
    <body>
        <%@include file="navbar.jsp"%>
        <div style="height:125px; width:100%; clear:both;"></div>
        <h1>Hello ${sessionScope.TRANSPORTATIONCOMPANY.fullName}
        <h1>Hello ${sessionScope.TRANSPORTATIONCOMPANY.phone}
        <h1>Hello ${sessionScope.TRANSPORTATIONCOMPANY.address}
        <h1>Hello ${sessionScope.TRANSPORTATIONCOMPANY.email}
        </h1>
        <form action="LogoutServlet" method="POST">
             <input type="submit" value="Logout" name="btnAction" />
        </form>
       
    </body>
</html>
