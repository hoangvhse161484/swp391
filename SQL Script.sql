﻿--Tạo Table
Create database SWP391_Database
GO


--Sử dụng Table
USE SWP391_Database;
GO

-- execute từng Table
---tạo role
CREATE TABLE [tblRoles](
	[roleID] INT IDENTITY(1,1) PRIMARY KEY,
	[roleName] [nvarchar](50) 
)

---tạo vị trí ngồi
CREATE TABLE [tblPosSeat](
	[seatPosID] INT IDENTITY(1,1) PRIMARY KEY,
	[seatPosName] [nvarchar] (50)
)

---tạo loại xe
CREATE TABLE [tblCarType](
	[carTypeID] INT IDENTITY(1,1) PRIMARY KEY,
	[carTypeName] [nvarchar] (50),
	[numberOfSeat] [nvarchar] (300)
)

---tạo chương trình chuyến đi
CREATE TABLE [tblDestination](
	[desID] INT IDENTITY(1,1) PRIMARY KEY,
	[startDes] [nvarchar] (100),
	[endDes] [nvarchar] (100),
	[startTime] [DATETIME],
	[arriveTime] [DATETIME],
)

---tạo danh sách các tài xế
CREATE TABLE [tblDriver](
	[driverID] INT IDENTITY(1,1) PRIMARY KEY,
	[driverName] [nvarchar] (100),
	[dob] [DATE],
	[address] [nvarchar] (50),
	[nationality] [nvarchar] (50),
)

---tạo số lượng
CREATE TABLE [tblTypeOfPrice](
	[TypeID] INT IDENTITY(1,1) PRIMARY KEY,
	[TypeName] [nvarchar] (100),
)

---tạo giá tiền và tổng số tiền phải thanh toán
CREATE TABLE [tblPrice](
	[priceID] INT IDENTITY(1,1) PRIMARY KEY,
	[price] FLOAT,
	[amount] INT,
	[TypeID] INT FOREIGN KEY REFERENCES [tblTypeOfPrice]([TypeID]),
	[totalPrice] FLOAT,
)

---tạo thông tin bằng lái của chủ xe
CREATE TABLE [tblLicenseVehicle](
	[licenseID] INT IDENTITY(1,1) PRIMARY KEY,
	[driverID] INT FOREIGN KEY REFERENCES [tblDriver](driverID),
	[Class] [nvarchar] (3),
	[ClassificationOfMotorVehicles] [nvarchar] (1000),
	[BeginningDate] [DATE],
	[Expires] [DATE]
)

---tạo khoản tiền cần thêm
CREATE TABLE [tblDeposit](
	[depositID] INT IDENTITY(1,1) PRIMARY KEY,
	[deposit] FLOAT
)

---tạo người dùng và cấp quyền
CREATE TABLE [tblUsers](
	[userID] INT IDENTITY(1,1) PRIMARY KEY,
	[fullName] [nvarchar] (50),
	[dob] [DATE],
	[address] [nvarchar] (50),
	[phone] [nvarchar] (10),
	[email] [nvarchar] (50),
	[wallet] INT FOREIGN KEY REFERENCES [tblDeposit](depositID),
	[password] [nvarchar](50),
	[roleID] INT FOREIGN KEY REFERENCES [tblRoles](roleID)
)

---tạo thông tin xe thuê
CREATE TABLE [tblVehicle](
	[vehicleID] INT IDENTITY(1,1) PRIMARY KEY,
	[vehicleName] [nvarchar] (50),
	[licensePlates] [nvarchar] (50),
	[carTypeID] INT FOREIGN KEY REFERENCES [tblCarType](carTypeID)
)

---tạo thông tin chuyến đi
CREATE TABLE [tblTrip](
	[tripID] INT IDENTITY(1,1) PRIMARY KEY,
	[tripName] [nvarchar] (50),
	[description] [nvarchar] (50),
	[desID] INT FOREIGN KEY REFERENCES [tblDestination](desID)
)

---tạo vé xe
CREATE TABLE [tblTicket](
	[ticketID] INT IDENTITY(1,1) PRIMARY KEY,
	[vehicleID] INT FOREIGN KEY REFERENCES [tblVehicle](vehicleID),
	[tripID] INT FOREIGN KEY REFERENCES [tblTrip](tripID),
	[priceID] INT FOREIGN KEY REFERENCES [tblPrice](priceID),
	[seatPosID] INT FOREIGN KEY REFERENCES [tblPosSeat](seatPosID),
	[userID] INT FOREIGN KEY REFERENCES [tblUsers](userID)
)

---tạo thông tin thuê xe + người thuê
CREATE TABLE [tblCarRental](
	[rentCarID] INT IDENTITY(1,1) PRIMARY KEY,
	[vehicleID] INT FOREIGN KEY REFERENCES [tblVehicle](vehicleID),
	[priceID] INT FOREIGN KEY REFERENCES [tblPrice](priceID),
	[licenseID] INT FOREIGN KEY REFERENCES [tblLicenseVehicle](licenseID),
	[Status] BIT,
	[userID] INT FOREIGN KEY REFERENCES [tblUsers](userID)
)

---tạo thông tin thanh toán
CREATE TABLE [tblPayment](
	[paymentID] INT IDENTITY(1,1) PRIMARY KEY,
	[paymentName] [nvarchar] (100),
	[priceID] INT FOREIGN KEY REFERENCES [tblPrice](priceID),
	[rentCarID] INT FOREIGN KEY REFERENCES [tblCarRental](rentCarID) NULL,
	[ticketID] INT FOREIGN KEY REFERENCES [tblTicket](ticketID) NULL
)