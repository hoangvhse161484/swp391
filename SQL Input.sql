﻿--Sử dụng Table
USE SWP391_Database;
GO


--execute từng Table
---thêm role
GO
INSERT INTO tblRoles(roleName)
VALUES (N'US'), (N'AD'), (N'TC')

---Thêm các loại phương tiện
GO
INSERT INTO tblCarType(carTypeName, numberOfSeat)
VALUES (N'4 seater car', 4), (N'7 seater car', 7), (N'16 seater car', 16), (N'32 seater car', 32)

---Thêm vị trí chỗ ngồi
GO
INSERT INTO tblPosSeat(seatPosName)
VALUES (N'Seat 1'), (N'Seat 2'), (N'Seat 3'), (N'Seat 4'), (N'Seat 5'), (N'Seat 6'), 
       (N'Seat 7'), (N'Seat 8'), (N'Seat 9'), (N'Seat 10'), (N'Seat 11'), (N'Seat 12'), 
	   (N'Seat 13'), (N'Seat 14'), (N'Seat 15'), (N'Seat 16'), (N'Seat 17'), (N'Seat 18'),
	   (N'Seat 19'), (N'Seat 20'), (N'Seat 21'), (N'Seat 22'), (N'Seat 23'), (N'Seat 24'),
	   (N'Seat 25'), (N'Seat 26'), (N'Seat 27'), (N'Seat 28'), (N'Seat 29'), (N'Seat 30'),
	   (N'Seat 31'), (N'Seat 32')

---Thêm địa điểm và thời gian chuyến đi
GO
INSERT INTO tblDestination(startDes, endDes, startTime, arriveTime)
VALUES (N'HCM', N'Da Nang', N'2022-10-01 06:00:00', N'2022-10-01 22:00:00'),
	   (N'HCM', N'Can Tho', N'2022-10-01 06:00:00', N'2022-10-01 10:00:00'),
	   (N'HCM', N'Nha Trang', N'2022-10-01 06:00:00', N'2022-10-01 18:00:00'),
	   (N'HCM', N'Phu Quoc', N'2022-10-01 06:00:00', N'2022-10-01 20:00:00'),
	   (N'HCM', N'Binh Duong', N'2022-10-01 06:00:00', N'2022-10-01 09:00:00'),
	   (N'HCM', N'Vinh', N'2022-10-01 06:00:00', N'2022-10-03 06:00:00'),
	   (N'HCM', N'Ha Noi', N'2022-10-01 06:00:00', N'2022-10-05 22:00:00')

---Thêm thông tin chuyến đi
GO
INSERT INTO tblTrip(tripName, description, desID)
VALUES (N'HCM to Da Nang', N'abc', 1),
	   (N'HCM to Can Tho', N'abc', 2),
	   (N'HCM to Nha Trang', N'abc', 3),
	   (N'HCM to Phu Quoc', N'abc', 4),
	   (N'HCM to Binh Duong', N'abc', 5),
	   (N'HCM to Vinh', N'abc', 6),
	   (N'HCM to Ha Noi', N'abc', 7)

GO
INSERT INTO tblDeposit(deposit)
VALUES (10000000), (20000000), (30000000), (40000000), (50000000), (60000000), (70000000), (80000000)

---Thêm thông tin người dùng + role
GO
INSERT INTO tblUsers(fullName, dob, address, phone, email, wallet, password, roleID)
VALUES (N'Hung', '2000-01-02', '123 ABC', 1111111111, N'abc@gmail.com', 1, 1, 2),
       (N'Hoang', '2001-12-02', '123 ABC', 2222222222, N'abc1@gmail.com', 2, 1, 3),
	   (N'Khang', '2000-02-02', '123 ABC', 3333333333, N'abc2@gmail.com', 3, 1, 2),
	   (N'Thinh', '2002-02-02', '123 ABC', 4444444444, N'abc3@gmail.com', 4, 1, 3),
	   (N'Nhan', '1999-02-02', '123 ABC', 5555555555, N'abc4@gmail.com', 5, 1, 1),
	   (N'Dat', '1998-02-02', '123 ABC', 6666666666, N'abc5@gmail.com', 6, 1, 1),
	   (N'Vu', '1997-02-02', '123 ABC', 7777777777, N'abc6@gmail.com', 7, 1, 1),
	   (N'Dung', '1996-02-02', '123 ABC', 8888888888, N'abc@7gmail.com', 8, 1, 1)

---Thêm tài xế
GO
INSERT INTO tblDriver(driverName, dob, address, nationality)
VALUES (N'Hung', '1980-01-02', '456 ABC', N'Viet Nam'),
       (N'Hoang', '1981-12-02', '456 ABC', N'Viet Nam'),
	   (N'Khang', '1980-02-02', '456 ABC', N'Viet Nam'),
	   (N'Thinh', '1982-02-02', '456 ABC', N'Viet Nam'),
	   (N'Nhan', '1989-02-02', '456 ABC', N'Viet Nam'),
	   (N'Dat', '1988-02-02', '456 ABC', N'Viet Nam'),
	   (N'Vu', '1987-02-02', '456 ABC', N'Viet Nam'),
	   (N'Dung', '1986-02-02', '456 ABC', N'Viet Nam')

---Thêm thông tin bằng lái cho tài xế
GO
INSERT INTO tblLicenseVehicle(driverID, Class, ClassificationOfMotorVehicles, BeginningDate, Expires)
VALUES (1, N'B11', N'Motor vehicle with automatic transmission, 
		having a permisbile maximun mass not exceeding 3500kgs and 
		not more than 9 seats; not used for commercial purpose', '2022-01-02', '2035-01-02'),

	   (2, N'B2', N'Truck, tractor with a trailer exceeding 3500kgs and vehicles classes B1, B2', '2026-12-02', '2036-12-02'),

	   (3, N'B12', N'Motor vehicle used for the carriage of passengers, 
		having a permisbile maximun mass not exceeding 3500kgs and 
		not more than 9 seats; not used for commercial purpose', '2022-02-02', '2035-02-02'),

	   (4, N'B2', N'Truck, tractor with a trailer exceeding 3500kgs and vehicles classes B1, B2', '2022-02-02', '2037-02-02'),

	   (5, N'B11', N'Motor vehicle with automatic transmission, 
		having a permisbile maximun mass not exceeding 3500kgs and 
		not more than 9 seats; not used for commercial purpose', '2022-02-02', '2044-02-02' ),

	   (6, N'B2', N'Truck, tractor with a trailer exceeding 3500kgs and vehicles classes B1, B2', '2022-02-02', '2043-01-02'),

	   (7, N'B12', N'Motor vehicle used for the carriage of passengers, 
		having a permisbile maximun mass not exceeding 3500kgs and 
		not more than 9 seats; not used for commercial purpose', '2022-02-02', '2042-02-02'),

	   (8, N'B2', N'Truck, tractor with a trailer exceeding 3500kgs and vehicles classes B1, B2', '2022-02-02', '2041-01-02')

---Thêm phương tiện du lịch + thông tin chỗ ngồi
GO
INSERT INTO tblVehicle(vehicleName, licensePlates, carTypeID)
VALUES (N'KIA', N'55A-11111', 1), (N'Hyundai', N'55A-22222', 2),
	   (N'Toyota', N'55A-33333', 1), (N'Honda', N'55A-44444', 2),
	   (N'KIA', N'55A-55555', 1), (N'Hyundai', N'55A-66666', 2), 
	   (N'Toyota', N'55A-77777', 1), (N'Honda', N'55A-88888', 2),
	   (N'KIA', N'50A-11111', 3), (N'Hyundai', N'50A-22222', 3),
	   (N'Toyota', N'50A-33333', 3), (N'Honda', N'50A-44444', 3),
	   (N'KIA', N'50A-55555', 4), (N'Hyundai', N'50A-66666', 4), 
	   (N'Toyota', N'50A-77777', 4), (N'Honda', N'50A-88888', 4)

---Thêm danh sách giá theo loại xe
GO 
INSERT INTO tblTypeOfPrice(TypeName)
VALUES (N'RentalCar'), (N'Ticket')

---Thêm thông tin giá, số lượng, 
GO 
INSERT INTO tblPrice(Price, amount, TypeID, totalPrice)
VALUES (6000000.0, 1, 1, 24000000.0), (7000000.0, 2, 2, 35000000.0), (8000000.0, 1, 1, 48000000.0), (9000000.0, 2, 2, 144000000.0)

---Thêm người dùng thuê xe + giá xe (mệnh giá VND)
GO
INSERT INTO tblCarRental(vehicleID, priceID, licenseID, Status, userID)
VALUES (1, 1, 1, 1, 5), (5, 2, 2, 0, 6), 
	   (2, 3, 3, 1, 7), (6, 4, 4, 1, 8)

---Thêm người dùng mua vé + giá vé (mệnh giá VND)
GO
INSERT INTO tblTicket(vehicleID, tripID, priceID, seatPosID, userID)
VALUES (10, 1, 3, 1, 5),
	   (16, 2, 4, 2, 7)
